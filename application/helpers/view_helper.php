<?php

use Jenssegers\Blade\Blade;

if (!function_exists('view')) {
	function view($view, $data = []) {
		$path  = APPPATH . 'views';
		$blade = new Blade($path, $path . '/cache');

		$blade->compiler()->directive('error', function ($msg){
			return '<p>Error :'.$msg.'</p>';
		});

		echo $blade->make($view, $data);
	}
}