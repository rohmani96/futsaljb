<?php $__env->startSection('title', 'JB Futsal | Inovice'); ?>



<?php $__env->startSection('content'); ?>
<div class="container">
<div class="page-section">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class='page-section padding-top-none panel panel-default paper-shadow' data-z="0.5">
                <div class='s-container'>
                    <h1 class='text-display-1 margin-top-none'>INOVICE BOOKING</h1>
                </div>
                <table id="table" class="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
             
              <th>Tgl Pesan</th>
              <th>Tgl Bayar</th>
              <th>Atas Nama</th>
              <th>Jumlah</th>
              <th>Status</th>
              <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                     <th style="width: 10px">No</th>
              <th>Tgl Pesan</th>
              <th>Tgl Bayar</th>
              <th>Atas Nama</th>
              <th>Jumlah</th>
              <th>Status</th>
              <th>Aksi</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                                "url": "<?php
                echo base_url().'datainovice'
                ?>",
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
 
        });
 
    });
</script>
 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.members', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>