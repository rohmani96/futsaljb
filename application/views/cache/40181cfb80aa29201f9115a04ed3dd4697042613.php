<?php $__env->startSection('title', 'Admin | Lapangan'); ?>

<?php $__env->startSection('content'); ?>
<?php
  $CI = &get_instance();
  echo $CI->session->flashdata("msg");
?>

<div class="row">
	<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Data inovice pengguna</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="table" class="table" cellspacing="0" width="100%">
            <thead>
                <tr>
<th style="width: 10px">Kode</th>
              <th>Tgl Pesan</th>
              <th>Tgl Bayar</th>
              <th>Atas Nama</th>
              <th>Subtotal</th>
              <th>Status</th>
              <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th style="width: 10px">Kode</th>
              <th>Tgl Pesan</th>
              <th>Tgl Bayar</th>
              <th>Atas Nama</th>
              <th>Subtotal</th>
              <th>Status</th>
              <th>Aksi</th>
                </tr>
            </tfoot>
        </table>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informasi Inovice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="tablemodal">
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" id="form-lunas">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pelunasan Inovice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="jumlah"></p>
        <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Atas Nama</label>
                    <input type="text" class="form-control" name="anama" id="nama" required>
                  </div>
        <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Jumlah Bayar</label>
                    <input type="text" class="form-control" name="jml" id="jml" >
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).on('click', '.btn-info', function(){
     _this = $(this);
      link   = _this.attr('data-url');
      kd     = _this.attr('data-id');
      $.ajax({ 
      type: 'GET', 
      url: link+'inovice/detail/'+kd,  
      dataType: 'json',
      success: function (data) {
        $('#tablemodal').empty();
        var aksi;
        $.each(data, function(i){
          if(data[i].norek!=''){
            aksi = '<form action="'+link+'pembayaran/unprocced" method="POST"><input type="hidden" name="kdinv" value="'+data[i].kdInv+'"><input type="hidden" name="id" value="'+data[i].id+'"><button type="submit" class="btn btn-info"><i class="fa fa-upload"></i></button></form';
          }else{
            aksi = '<a href="'+link+'pembayaran/hapus/'+data[i].id+'" class="btn btn-danger"><i class="fa fa-trash"></i></a>&nbsp;';
          }
          $('#tablemodal').append('<tr><td><a href="'+link+'assets/images/payment/'+data[i].gambar+'" target="_blank"><img src="'+link+'assets/images/payment/'+data[i].gambar+'" style="height:50px"></a><td>'+data[i].atasnama+'</td><td>Rp.'+data[i].jmlBayar+'</td><td>'+data[i].norek+'</td><td>'+aksi+'</td></tr>');
        });
      }
      });
      
  });

  $(document).on('click', '.btn-danger', function(){
      if (!confirm('Apakah Anda Yakin?')) {
          event.preventDefault();
      }
    });

  $(document).on('click', '.btn-primary', function(){
    _this         = $(this);
    id  = _this.attr('data-id');
    jml = _this.attr('data-jml');
    url = _this.attr('data-url');
    $('#form-lunas').attr('action', url+'inovice/bayar/'+ id);
    $('#jumlah').text('Jumlah yang harus dibayarkan adalah: Rp.'+parseInt(jml) *50/100);
     $('#jml').val(parseInt(jml) *50/100)

    
  });

  var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                                "url": "<?php
                echo base_url().'admin/datainovice'
                ?>",
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
 
        });
 
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>