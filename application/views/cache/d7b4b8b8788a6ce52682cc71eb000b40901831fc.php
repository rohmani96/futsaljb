<?php $__env->startSection('title', 'JB Futsal | Profile'); ?>

<?php $__env->startSection('content'); ?>
	 <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-8">
                <h4 class="page-section-heading"> TENTANG FUTSAL JB CIKARANG </h4>
                <div class="page-section">
                    <div class="width-350 width-300-md width-100pc-xs paragraph-inline">
                        <div class="embed-responsive embed-responsive-16by9">
                          <img src='<?php echo e(base_url()); ?>assets/images/profil.jpg'>
                        </div>
                    </div>
                    <p>
                        Lapangan futsal mulai menjamur di kota kota besar di Indonesia dan hal ini menjadi lahan bisnins yang cukup menjanjikan. pertumbuhan bisnis futsal di Indonesia cukup siignifikan, kalau boleh di biang luar biasa. tidak hanya di jakarta, Surabaya, Medan, Palembang, dan kota kota besar yang lainnya.
                        </p>
                    <p>
                        Futsal menjadi jawaban atas sempitnya lahan berman di daerah perkotaan. saat ini banyak lahan kosong dan lapangan olahraga lain yang berubah menjadi lapangan futsal. Lapangan itu tidak hanya dimiliki oleh pemda melalui gedung gedung GOR, tetapi makin hari makin banyak pula pengusaha yang mencoba terjun ke bisnis ini.
                    </p>
                    <br/>
                    <p>
                        
                    <br/>
                   
                </div>
               
                    <!-- // END .panel -->
                </div>
                <!-- // END .page-section -->
            </div>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.members', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>