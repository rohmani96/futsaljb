<?php $__env->startSection('title', 'Admin | Member'); ?>

<?php $__env->startSection('content'); ?>
<div >
     <button class="btn btn-app" style="background-color: white;" data-toggle="modal" data-target="#myModal">
          <i class="fa fa-plus-square-o"></i> Add User
    </button>

</div>
<div class="row">
  
  <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <div class="col-md-2">
      <div class="card">
        <label class="label-info" style="position: absolute;">
        <?php if($data['role']==0): ?>
          Admin
        <?php else: ?> 
          User
        <?php endif; ?></label>
          <img class="card-img-top" src="<?php echo e(base_url().'assets/images/people/'.$data['foto']); ?>" alt="Card image">
          <div class="card-body">
            <p class="card-title"><b><?php echo e($data['nmPengguna']); ?></b></p>
            <p><?php echo e($data['kontak']); ?></p>
            <button class="btn btn-primary">See Profile</button>
          </div>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <div class="col-md-12">
 <center><?php
  echo $page;
 ?> </center>
</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?php echo e(base_url().'user/save/admin'); ?>" enctype="multipart/form-data" method="POST">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="form-group">
                <label for="wiz-lusername">Username</label>
            <input class="form-control" type="text" id="wiz-lusername" name="txtUsername" required />

        </div>
           <div class="form-group">
            <label for="wiz-lpass">Password</label>
            <input class="form-control" type="password" id="wiz-lpass" name="txtPass" required />  
        </div>

        <div class="form-group">
             <label for="wiz-email">Email</label>
            <input name="txtEmail" class="form-control" type="email" id="wiz-email" required />
           
        </div>

         <div class="form-group ">
           <label for="wiz-lname">Nama Lengkap</label>
          <input class="form-control" type="text" id="wiz-lname" name="txtNmLengkap" required />
         
          
      </div>  
       
      <div class="form-group">
          <label for="wiz-address">Alamat</label>
          <textarea name="txtAlamat" rows="3" class="form-control" id="wiz-address" placeholder="Alamat"></textarea>
        
      </div>             
      
      
      <div class="form-group">
          <label for="wiz-nohp1">Handphone</label>
          <input name="txtKontak" class="form-control" type="text" id="wiz-nohp1" placeholder="Nomor HP/ Telpon" />
          
      </div>
                   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>