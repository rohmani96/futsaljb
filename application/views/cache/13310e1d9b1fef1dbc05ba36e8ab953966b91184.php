<?php $__env->startSection('title', 'JB Futsal | Riwayat booking'); ?>

<?php $__env->startSection('content'); ?>
	 <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-12 col-lg-12">
				 <h4 class="page-section-heading"> DAFTAR RIWAYAT BOOKING LAPANGAN FUTSAL</h4>
                    <div class="panel panel-default">
                        <!-- Data table -->
                        <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                             <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                     	    <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                    </tfoot>
                                    <tbody>
                                    	<?php
                                    		$no =1;
                                    	?>
                                    	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr>
                                            <td><?php echo e($no++); ?></td>
											<td><?php echo e($data['kdLap']); ?></td>
                                            <td><?php echo e($data['tglBook']); ?></td>
                                            <td><?php echo e($data['jamBook'].' - '.$data['jamSelesai']); ?></td>
											<td>Rp.<?php echo e($data['harga']); ?></td>
                                            <td><?php echo e($data['status']); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                        </table>
                        <!-- // Data table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.members', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>