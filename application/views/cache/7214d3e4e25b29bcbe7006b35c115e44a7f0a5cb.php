<?php $__env->startSection('title', 'Admin | Dashboard'); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo e($user); ?></h3>

        <p>Jumlah Member</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-contact"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo e($jadwal); ?></h3>

        <p>Jadwal Hari ini</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-briefcase-outline"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo e($inovice); ?></h3>

        <p>Inovice Belum Lunas</p>
      </div>
      <div class="icon">
        <i class="ion ion-location"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

   <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo e($booking); ?></h3>

        <p>Jumlah Booking</p>
      </div>
      <div class="icon">
        <i class="ion ion-location"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-md-12">
    <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Data Pembayaran Terbaru</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered">
            <tr>
              <th>Bukti Pembayaran</th>
              <th>No Inovice</th>
              <th>Atas Nama</th>
              <th>No Rekening</th>
              <th>Jumlah Bayar</th>
              <th>Action</th>
            </tr>
            
              <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><a href="<?php echo e(base_url().'assets/images/payment/'.$data['gambar']); ?>" target="_blank"><img src="<?php echo e(base_url().'assets/images/payment/'.$data['gambar']); ?>" style="height: 50px;"></a></td>
                <td>INV/<?php echo e($data['kdInv']); ?></td>
                <td><?php echo e($data['atasnama']); ?></td>
                <td><?php echo e($data['norek']); ?></td>
                <td>Rp.<?php echo e($data['jmlBayar']); ?></td>
                <td>
                  <form action="<?php echo e(base_url()); ?>pembayaran/valid/<?php echo e($data['id']); ?>" method="POST" style="display: inline;">
                  <input type="hidden" name="kdInv" value="<?php echo e($data['kdInv']); ?>">
                  <button type="submit" class="btn btn-success">Valid</button>
                  </form>
                  <a href="<?php echo e(base_url()); ?>pembayaran/valid/<?php echo e($data['id']); ?>" class="btn btn-danger">Tidak Valid</a>
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </table>
    </div>
  </div>
  </div>
  </div>
</div>

<script type="text/javascript">
  function alert(kondisi) {
    if (!confirm('Apakah anda yakin ini '+kondisi+'?')) {
      event.preventDefault();
    }
    // body...
  }
</script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>