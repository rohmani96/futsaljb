<?php $__env->startSection('title', 'JB Futsal | Keranjang'); ?>



<?php $__env->startSection('content'); ?>
<div class="container">
<div class="page-section">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class='page-section padding-top-none'>
                <div class='s-container'>
                    <h1 class='text-display-1 margin-top-none'>KERANJANG BOOKING</h1>
                </div>
                <div class="col-md-8" style="padding-bottom: 20px;">
                  <div class="row">
                	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<div class="col-md-11 col-xs-12 card-chart" style="margin-bottom: 10px;">
                  <div class="col-md-4 col-xs-12">
      					  <img src="<?php echo e(base_url().'assets/images/fields/'.$data['gambar']); ?>" alt="Avatar" style="width: 100%">
                  </div>
      					  <div class="col-md-7 col-xs-10">
      					    <h4><b>Lapangan No. <?php echo e($data['kdLap']); ?></b></h4> 
      					    <p>Jam Booking    : <?php echo e($data['jamBook'].' - '. $data['jamSelesai']); ?></p> 
      					    <p>Tanggal Booking: <?php echo e($data['tglBook']); ?></p>
      					    <p>Harga Booking  : Rp.<?php echo e($data['harga']); ?></p>
      					  </div>
      					 <div class="form-check col-md-1 col-xs-2">
                      <?php if(strtotime($data['tglBook'].' '.$data['jamBook'])> time()): ?>
      					       <label class="customcheck">
      				          <input type="checkbox" data-hrg="<?php echo e($data['harga']); ?>" data-id="<?php echo e($data['kdBokingTemp']); ?>" data-val="<?php echo e($data['kdJadwal']); ?>">
      				          <span class="checkmark"></span>
      				        </label><br>
                      <?php else: ?>
                      <p style="color: red">Jadwal Tidak Valid</p>
                      <?php endif; ?>
                      <button class="btn btn-danger" data-link='<?php echo e(base_url()); ?>keranjang/hapus/<?php echo e($data['kdJadwal']); ?>'><i class="fa fa-trash"></i></button>
      					  </div>
      					</div>
      					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
              </div>
                <div class="col-md-4" style="background-color: white;padding-top: 10px;">
                	<div>
                		<table class="table table-bordered">
                			<tr style="width: 50%">
                				<td><label>Total Booking:</label></td>
                				<td><span id="jumlah">0</span></td>
                			</tr>
                			<tr>
                				<td><label>Subtotal :</label></td>
                				<td>Rp.<span id="harga">0</span></td>
                			</tr>
                		</table>
                	</div>
                	<div>
                	<ul class="nav nav-tabs">
					    <li class="active" style="width: 50%"><a href="#">Data Profil</a></li>
					    <li style="width: 50%"><a href="#">Data Baru</a></li>
				  	</ul>
				  	<div style="padding: 10px;">
				  		<form action="<?php echo e(base_url()); ?>booking/inovice" method="POST">
				  		<div class="form-group">
				  			<label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama"  placeholder="Nama Lengkap" value="<?php echo e($profile[0]['nmPengguna']); ?>">
                        </div>
                        <div class="form-group">
                        	<label>Email</label>
	                        <input type="email" class="form-control" name="email"  placeholder="Alamat Email" value="<?php echo e($profile[0]['emailPengguna']); ?>">
	                    </div>
	                    <div class="form-group">
	                    	 <label>No.Hp/Telpon</label>
                            <input type="text" class="form-control" name="kontak"  placeholder="Nomor Telpon" value="<?php echo e($profile[0]['kontak']); ?>">
                        </div>
                        <div class="form-group">
                        	<label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="<?php echo e($profile[0]['alamatPengguna']); ?>">
                        </div>
                        <div id="laps">
                        

                        </div>
                         <div class="form-group form-control-material" style="border-bottom: none;">
                        <button type="submit" class="btn btn-primary pull-right" id="finish" style="display: none;">Selesai Booking</button>
                    </div>
                    	</form>
				  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">

	$('.form-check input').change(function(event) {
      _this = $(this);
      hrg 	= parseInt(_this.attr('data-hrg'));
      id 	= _this.attr('data-id');
      val   = _this.attr('data-val');
      total = parseInt($('#harga').text());
      jumlah = parseInt($('#jumlah').text());
      
      if(_this.is( ":checked" )){
      	subtotal = total+hrg;
      	subjumlah= jumlah+1;
       
      	$('#harga').text(parseInt(subtotal));
      	$('#jumlah').text(parseInt(subjumlah));
      	$('#laps').append('<input type="hidden" name="idbook[]" id="lap'+id+'" value="'+val+'">');
        $('#finish').show();
      }else{
      	subtotal = total-hrg;
      	subjumlah= jumlah-1;
       
      	$('#harga').text(parseInt(subtotal));
      	$('#jumlah').text(parseInt(subjumlah));
      	$('#lap'+id).remove();
        if(jumlah <= 1){
           $('#finish').hide();
        }
      }

    });

     $(document).on('click', '.btn-danger', function(){
      _this = $(this);
      link = _this.attr('data-link');
      if (confirm('Apakah Anda Yakin?')) {
          window.location = link;
      }
    });
</script>
 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.members', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>