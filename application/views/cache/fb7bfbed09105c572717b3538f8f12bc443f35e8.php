<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title> <?php echo $__env->yieldContent('title'); ?></title>
    <link href="<?php echo e(base_url()); ?>assets/css/vendor.min.css" rel="stylesheet">
    <link href="<?php echo e(base_url()); ?>assets/css/theme-core.min.css" rel="stylesheet">
    <link href="<?php echo e(base_url()); ?>assets/css/module-essentials.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-material.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-layout.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-sidebar.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-sidebar-skins.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-navbar.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-messages.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-carousel-slick.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-charts.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-maps.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-colors-alerts.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-colors-background.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-colors-buttons.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/module-colors-text.min.css" rel="stylesheet" />
    <link href="<?php echo e(base_url()); ?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</head>
<body>

    <!-- navbar navbar navbar -->
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
      <div class='container'>
            <div class='navbar-header'>
                <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#main-nav'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <div class='navbar-brand navbar-brand-logo'>
                   <img src='<?php echo e(base_url()); ?>assets/images/logo.png'>
                </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class='collapse navbar-collapse' id='main-nav'>
                <ul class='nav navbar-nav navbar-nav-margin-left'>
                    <li class="
                    <?php if($menu == 'home'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>">
                        <a href="<?php echo e(base_url()); ?>">Home</a>
                    </li> 
                    <?php if(!isset($_SESSION['online'])): ?>
                        <li class="
                    <?php if($menu == 'profile'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>" ><a href="<?php echo e(base_url().'profile'); ?>">Profil </a></li>
                        <li class="
                    <?php if($menu == 'lapangan'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>"><a href="<?php echo e(base_url().'lapangan'); ?>">Lapangan </a></li>
                    <?php else: ?>
                        <li class="
                    <?php if($menu == 'booking'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>"><a href="<?php echo e(base_url().'booking'); ?>">Booking</a></li>
                        <li class="
                    <?php if($menu == 'inovice'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>"><a href="<?php echo e(base_url().'inovice'); ?>">Invoice</a></li>
                    <?php endif; ?>
                    <li class="
                    <?php if($menu == 'jadwal'): ?>
                    <?php echo e('active'); ?>

                    <?php endif; ?>"><a href="<?php echo e(base_url().'jadwal'); ?>">Jadwal</a></li> 
                </ul>
                <div class='navbar-right'>
                    <ul class='nav navbar-nav navbar-nav-bordered navbar-nav-margin-right'>
                        <?php if(isset($_SESSION['online'])): ?>
                            <li class='dropdown user'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                                <i class="fa fa-shopping-cart"></i>
                                <span class="label label-success"><?php echo e(count($booking)); ?></span></a>
                            <ul class='dropdown-menu' role='menu'>
                                <li style="font-size: 12px"><?php echo e(count($booking)); ?> pesanan belum diproses</li>
                                <?php $__currentLoopData = $booking; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>No Lap <?php echo e($book['kdLap']); ?>/ <?php echo e($book['tglBook']); ?>/ <?php echo e($book['jamBook']); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <li><a style="font-size: 12px; border-top:0px;" href='<?php echo e(base_url().'keranjang'); ?>'></i> Selengkapnya</a></li>
                            </ul>
                            </li> 
                            <li class='dropdown user'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                            <?php if(isset($_SESSION['foto'])): ?>
                                <img src='<?php echo e(base_url().'assets/images/people/' . $_SESSION['foto']); ?>' alt='' class='img-circle' /> <?php echo e($_SESSION['nama']); ?><span class='caret'></span></a>
                            <?php else: ?>
                                <img src='<?php echo e(base_url().'assets/images/people/profile.png'); ?>' alt='' class='img-circle' /><?php echo e($_SESSION['nama']); ?><span class='caret'></span></a>
                            <?php endif; ?>
                            <ul class='dropdown-menu' role='menu'>
                                <li><a href='<?php echo e(base_url().'pengguna/'. $_SESSION['iduser']); ?>'><i class='fa fa-user'></i> Profil & History</a></li>
                                <li><a href='<?php echo e(base_url().'signout'); ?>'><i class='fa fa-sign-out'></i> Logout</a></li>
                            </ul>
                            </li> 
                    </ul>
                        <?php else: ?>
                            <a href='<?php echo e(base_url().'signin'); ?>' class='navbar-btn btn btn-primary'>Sign In</a>
                        <?php endif; ?>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </div>

    <!-- content content content -->
    <?php echo $__env->yieldContent('content'); ?>
   
    <!-- Footer -->
    <!-- // Footer -->
    <!-- Inline Script for colors and config objects; used by various external scripts; -->
    <footer class="footer">
      <strong>JB Futsal - </strong> Cikarang <STRONG>|Design By : </STRONG>Kiko Alfianto</STRONG>  &copy; Copyright 2017
   </footer>
   <script>
    var colors = {
        "danger-color": "#e74c3c",
        "success-color": "#81b53e",
        "warning-color": "#f0ad4e",
        "inverse-color": "#2c3e50",
        "info-color": "#2d7cb5",
        "default-color": "#6e7882",
        "default-light-color": "#cfd9db",
        "purple-color": "#9D8AC7",
        "mustard-color": "#d4d171",
        "lightred-color": "#e15258",
        "body-bg": "#f6f6f6"
    };
    var config = {
        theme: "html",
        skins: {
            "default": {
                "primary-color": "#42a5f5"
            }
        }
    };
    </script>
    <!-- Separate Vendor Script Bundles -->
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-core.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-countdown.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-tables.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-forms.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-carousel-slick.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-player.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-charts-flot.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/vendor-nestable.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-essentials.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-material.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-layout.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-sidebar.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-carousel-slick.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-player.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-messages.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/module-charts-flot.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/theme-core.min.js"></script>

</body>
</html>