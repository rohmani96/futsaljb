<?php $__env->startSection('title', 'Admin | Lapangan'); ?>

<?php $__env->startSection('content'); ?>
<?php
  $CI = &get_instance();
  echo $CI->session->flashdata("msg");
?>

<div >
     <button class="btn btn-app" style="background-color: white;" data-toggle="modal" data-target="#myModal">
          <i class="fa fa-plus-square-o"></i> Add New
    </button>

</div>
<div class="row">
	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="col-md-6">
         <div class="card border-primary flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
               <strong class="d-inline-block mb-2 text-primary">Lapangan:<?php echo e($data['kdLap']); ?><br>Harga : <?php echo e($data['harga']); ?></strong>
               <p class="card-text mb-auto"><?php echo e($data['deskripsi']); ?></p>
               <button class="btn btn-outline-primary btn-sm border-primary" data-toggle="modal" data-target="#editModal" data-url="<?php echo e(base_url()); ?>" data-kd="<?php echo e($data['kdLap']); ?>" data-hrg="<?php echo e($data['harga']); ?>" data-desc="<?php echo e($data['deskripsi']); ?>">Edit</button>
               <button class="btn btn-outline-danger btn-sm border-danger" onclick="del('<?php echo e(base_url().'lapangan/hapus/'.$data['kdLap']); ?>')">Hapus</button>
            </div>
            <img class="card-img-right flex-auto d-none d-lg-block" alt="Thumbnail [200x250]" src="<?php echo e(base_url().'assets/images/fields/'.$data['gambar']); ?>" style="width: 200px; height: 200px;">
         </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?php echo e(base_url().'lapangan/simpan'); ?>" enctype="multipart/form-data" method="POST">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Lapangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">No Lapangan:</label>
            <input type="text" class="form-control" name="nomor">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Harga Perjam:</label>
            <input type="text" class="form-control" name="harga">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Deskripsi:</label>
            <textarea class="form-control" id="message-text" name="desc"></textarea>
          </div>
          <div class="form-group">
              <label for="exampleInputFile" class="col-form-label">Gambar:</label>
              <input type="file" name="filefoto">
 		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="#" enctype="multipart/form-data" method="POST" id="update_lap">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Lapangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">No Lapangan:</label>
            <input type="text" class="form-control" name="nomor" id="no_lap">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Harga Perjam:</label>
            <input type="text" class="form-control" name="harga" id="hrg_lap">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Deskripsi:</label>
            <textarea class="form-control" name="desc" id="desc_lap"></textarea>
          </div>
          <div class="form-group">
              <label for="exampleInputFile" class="col-form-label">Gambar:</label>
              <input type="file" name="filefoto">
 		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>
<script>
	 function edit(url, no, hrg, desc) {
	 	$('#update_lap').attr('action', url+'/lapangan/update/'+no);
	 	$('#no_lap').val(no);
    $('#hrg_lap').val(hrg);
	 	$('#desc_lap').val(desc);
	 }

   $(document).on('click', '.btn-outline-primary', function(){
    _this         = $(this);
    no  = _this.attr('data-kd');
    hrg = _this.attr('data-hrg');
    url = _this.attr('data-url');
    desc = _this.attr('data-desc');
// data-url
// data-kd
// data-hrg
// data-desc
    $('#update_lap').attr('action', url+'/lapangan/update/'+no);
    $('#no_lap').val(no);
    $('#hrg_lap').val(hrg);
    $('#desc_lap').val(desc);

    
  });

	 function del(url) {
	 	if (confirm('Are you sure you want to delete?')) {
      		window.href(url);
      	}
	 }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>