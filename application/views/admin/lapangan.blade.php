@extends('layout.admin')

@section('title', 'Admin | Lapangan')

@section('content')
@php
  $CI = &get_instance();
  echo $CI->session->flashdata("msg");
@endphp

<div >
     <button class="btn btn-app" style="background-color: white;" data-toggle="modal" data-target="#myModal">
          <i class="fa fa-plus-square-o"></i> Add New
    </button>

</div>
<div class="row">
	@foreach($datas as $data)
	<div class="col-md-6">
         <div class="card border-primary flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
               <strong class="d-inline-block mb-2 text-primary">Lapangan:{{ $data['kdLap'] }}<br>Harga : {{ $data['harga'] }}</strong>
               <p class="card-text mb-auto">{{ $data['deskripsi'] }}</p>
               <button class="btn btn-outline-primary btn-sm border-primary" data-toggle="modal" data-target="#editModal" data-url="{{ base_url() }}" data-kd="{{ $data['kdLap'] }}" data-hrg="{{ $data['harga'] }}" data-desc="{{ $data['deskripsi'] }}">Edit</button>
               <button class="btn btn-outline-danger btn-sm border-danger" onclick="del('{{ base_url().'lapangan/hapus/'.$data['kdLap'] }}')">Hapus</button>
            </div>
            <img class="card-img-right flex-auto d-none d-lg-block" alt="Thumbnail [200x250]" src="{{ base_url().'assets/images/fields/'.$data['gambar'] }}" style="width: 200px; height: 200px;">
         </div>
      </div>
      @endforeach
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="{{ base_url().'lapangan/simpan' }}" enctype="multipart/form-data" method="POST">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Lapangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">No Lapangan:</label>
            <input type="text" class="form-control" name="nomor">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Harga Perjam:</label>
            <input type="text" class="form-control" name="harga">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Deskripsi:</label>
            <textarea class="form-control" id="message-text" name="desc"></textarea>
          </div>
          <div class="form-group">
              <label for="exampleInputFile" class="col-form-label">Gambar:</label>
              <input type="file" name="filefoto">
 		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="#" enctype="multipart/form-data" method="POST" id="update_lap">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Lapangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">No Lapangan:</label>
            <input type="text" class="form-control" name="nomor" id="no_lap">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Harga Perjam:</label>
            <input type="text" class="form-control" name="harga" id="hrg_lap">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Deskripsi:</label>
            <textarea class="form-control" name="desc" id="desc_lap"></textarea>
          </div>
          <div class="form-group">
              <label for="exampleInputFile" class="col-form-label">Gambar:</label>
              <input type="file" name="filefoto">
 		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>
<script>
	 function edit(url, no, hrg, desc) {
	 	$('#update_lap').attr('action', url+'/lapangan/update/'+no);
	 	$('#no_lap').val(no);
    $('#hrg_lap').val(hrg);
	 	$('#desc_lap').val(desc);
	 }

   $(document).on('click', '.btn-outline-primary', function(){
    _this         = $(this);
    no  = _this.attr('data-kd');
    hrg = _this.attr('data-hrg');
    url = _this.attr('data-url');
    desc = _this.attr('data-desc');
// data-url
// data-kd
// data-hrg
// data-desc
    $('#update_lap').attr('action', url+'/lapangan/update/'+no);
    $('#no_lap').val(no);
    $('#hrg_lap').val(hrg);
    $('#desc_lap').val(desc);

    
  });

	 function del(url) {
	 	if (confirm('Are you sure you want to delete?')) {
      	 window.location = url;
      	}
	 }
</script>
@endsection