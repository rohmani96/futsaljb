@extends('layout.admin')

@section('title', 'Admin | Jadwal')

@section('content')
@php
  $CI = &get_instance();
  echo $CI->session->flashdata("msg");
@endphp
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Jadwal</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <table id="table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
             
              <th>Tgl Booking</th>
               <th>No Lapangan</th>
              <th>Jam Booking</th>
              <th>Status Booking</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                     <th style="width: 10px">No</th>
              <th>Tgl Booking</th>
               <th>No Lapangan</th>
              <th>Jam Booking</th>
              <th>Status Booking</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="#" enctype="multipart/form-data" method="POST" id="frm-edit">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Booking</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	 <p>Nama   :</p>
         <p>Alamat :</p>
         <p>Kontak :</p>
         <p>Kode Inovice:</p>
         <p>Status Bayar:</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
  </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	

  var table;
    $(document).ready(function() {
      $(document).on('click', '.btn-outline-primary', function(){
      _this = $(this);
      
      
    });
 
        //datatables
        table = $('#table').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                                "url": "@php
                echo base_url().'admin/databooking'
                @endphp",
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
 
        });
 
    });

</script>

@endsection