@extends('layout.members')

@section('title', 'JB Futsal | Login')

{{-- @if($this->session->flashdata('error_msg') != null)

@error ($this->session->flashdata('error_msg'))

@endif --}}

@section('content')
@php
  $CI = &get_instance();
  if($CI->session->flashdata("msg")!=null){
  echo '<div class="alert alert-danger">'.$CI->session->flashdata("msg").'</div>';
}
@endphp
 <div id="content" style="padding-top: 100px;">
        <div class="container-fluid">
            <div class="lock-container row">
                <div class="col-md-3"></div>
                <div class="col-md-6 panel panel-default text-center paper-shadow" data-z="0.5">
                    <h1 class="text-display-1 text-center margin-bottom-none">Sign In</h1>
                    <img src="{{ base_url().'assets/images/loginlogo.png' }}" class="img-circle width-80">
                    <div class="panel-body">
                    <form action="{{ base_url().'login_user' }}" enctype="multipard/form-data" method="POST">
                        <div class="form-group">
                            <div class="form-control-material">
                                <input class="form-control" id="username" name="txtUsername" type="text" placeholder="Enter Username" required>
                                <label for="username">Username</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-material">
                                <input class="form-control" id="password" name="txtPassword" type="password" placeholder="Enter Password" required>
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Login <i class="fa fa-fw fa-unlock-alt"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
</div>
@endsection