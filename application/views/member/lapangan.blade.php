@extends('layout.members')

@section('title', 'JB Futsal | Lapangan')

@section('content')

 <div class='container'>
        <div class='page-section'>
            <div class='row'>
                <div class='col-md-12 col-lg-12'>
                    <div class='page-section padding-top-none'>
                        <div class='s-container'>
                            <h1 class='text-display-1 margin-top-none'>INFO LAPANGAN</h1>

                        </div>
                        <br/>
                        <div class="row">
                        @foreach ($datas as $data)
                        <div class="col-md-6 col-sm-12">
                            <div class="card border-primary flex-md-row mb-4 shadow-sm h-md-250">
                                <div>
                                <img class="card-img-right flex-auto d-none d-lg-block" alt="Thumbnail [200x250]" src="{{ base_url().'assets/images/fields/'.$data['gambar'] }}" style="width: 200px; height: 200px;">
                                 </div>
                                <div class="card-body">
                                   <strong class="d-inline-block mb-2 text-primary">Lapangan No. {{ $data['kdLap'] }}</strong>
                                   <p class="card-text mb-auto">{{ $data['deskripsi'] }}</p>
                                </div>
                             </div>
                        </div>
                        @endforeach 
                        <div class="clear"></div>
                        </div> 
                    </div>                        
                </div>
            </div>
        </div>
    </div>
@endsection