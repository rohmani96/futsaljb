@extends('layout.members')

@section('title', 'JB Futsal | Riwayat booking')

@section('content')
	 <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-12 col-lg-12">
				 <h4 class="page-section-heading"> DAFTAR RIWAYAT BOOKING LAPANGAN FUTSAL</h4>
                    <div class="panel panel-default">
                        <!-- Data table -->
                        <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                             <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                     	    <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                    </tfoot>
                                    <tbody>
                                    	@php
                                    		$no =1;
                                    	@endphp
                                    	@foreach($datas as $data)
										<tr>
                                            <td>{{$no++}}</td>
											<td>{{$data['kdLap']}}</td>
                                            <td>{{$data['tglBook']}}</td>
                                            <td>{{$data['jamBook'].' - '.$data['jamSelesai']}}</td>
											<td>Rp.{{$data['harga']}}</td>
                                            <td>{{$data['status']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                        </table>
                        <!-- // Data table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection