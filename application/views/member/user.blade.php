@extends('layout.members')

@section('title', 'JB Futsal | Profile')

@section('content')
@php
  $CI = &get_instance();
  echo $CI->session->flashdata("msg");
@endphp
    <div class="container">
        <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <h3 class="site-title">My Profile</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                 <div class="panel">
                <div class=" panel-body ">    
                    <div class="col-md-5">
                        <img class="img-responsive" src="{{base_url()}}assets/images/people/{{$datas[0]['foto']}}" style="width:40%;margin-left: 30%;">
                        <div class="clearfix" style="text-align: center;">
                            <h3>{{$datas[0]['nmPengguna']}}</h3>
                            <h4>You are a Member</h4>
                             <button type="button" class="btn btn-success btn-md pull-center">Edit Profile</button>
                           <hr>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="profile-block">
                            <form action="{{base_url()}}pengguna/update/{{$datas[0]['kdPengguna']}}" method="POST"  enctype="multipart/form-data">
                            <div class="form-group " style="display: none;" id="name">
                                     <label for="wiz-lname">Nama Lengkap</label>
                                    <input class="form-control" type="text" id="wiz-lname" name="txtNmLengkap" value="{{$datas[0]['nmPengguna']}}" required />
                                   
                                    
                                </div> 

                                <div class="form-group">
                                     <label for="wiz-email">Email</label>
                                    <input name="txtEmail" class="form-control" type="email" id="wiz-email" value="{{$datas[0]['emailPengguna']}}" required />
                                   
                                </div>

                                <div class="form-group">
                                    <label for="wiz-nohp1">Handphone</label>
                                    <input name="txtKontak" class="form-control" type="text" id="wiz-nohp1" value="{{$datas[0]['kontak']}}" />
                                    
                                </div> 
                                 
                                <div class="form-group">
                                    <label for="wiz-address">Alamat</label>
                                    <textarea name="txtAlamat" rows="3" class="form-control" id="wiz-address">{{$datas[0]['alamatPengguna']}}</textarea>
                                  
                                </div>             
                                
                                
                                <div class="form-group" style="display: none;" id="photo">
                                
                                    <label for="wiz-photo">Upload Foto:</label>
                                
                                    <input class="form-control" type="file" id="wiz-photo"  name="upPhoto" />
                                  
                                </div>
                                <button type="submit" class="btn btn-info pull-right" id="update" style="display: none;">Update Profile</button>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <div class="row">
                 <div class="panel">
                <div class=" panel-body ">
                 <div class="col-md-12">
                    <h4>Booking History</h4>
                    
                     <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                             <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                            <th>#</th>
                                            <th>No Lapangan</th>
                                            <th>Tgl.Boking </th>
                                            <th>Jam</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                    </tfoot>
                                    <tbody>
                                        @php
                                            $no =1;
                                        @endphp
                                        @foreach($history as $data)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$data['kdLap']}}</td>
                                            <td>{{$data['tglBook']}}</td>
                                            <td>{{$data['jamBook'].' - '.$data['jamSelesai']}}</td>
                                            <td>Rp.{{$data['harga']}}</td>
                                            <td>{{$data['status']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                        </table>
               
                    </div>
                </div>
                        
                    
                </div>
                </div>
                </div>
               
               
            </div>
           
        </div>
         
         <div class="col-md-2"></div>
         </div>
     </div>

     <script type="text/javascript">
         
         $('.profile-block input, textarea').prop('disabled', true);

         $(document).on('click', '.btn-success', function(){
            _this = $(this);
         if(_this.text()=='Batal'){
            $('.profile-block input, textarea').prop('disabled', true);
            $('#name').hide();
            $('#photo').hide();
            $('#update').hide();
            _this.text('Edit Profile');
          }else{
              $('.profile-block input, textarea').prop('disabled', false);
              $('#name').show();
              $('#photo').show();
              $('#update').show();
              _this.text('Batal');
            }
          

        });
     </script>
@endsection