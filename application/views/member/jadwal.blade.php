@extends('layout.members')

@section('title', 'JB Futsal | Jadwal')

@section('content')
	 <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-12 col-lg-12">
				 <h4 class="page-section-heading"> DAFTAR JADWAL FUTSAL JB CIKARANG </h4>
                    <div class="panel panel-default">
                        <!-- Data table -->
                        <table id="table" class="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
             
              <th>Tgl Booking</th>
               <th>No Lapangan</th>
              <th>Jam Booking</th>
              <th>Status Booking</th>
       
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                     <th style="width: 10px">No</th>
              <th>Tgl Booking</th>
               <th>No Lapangan</th>
              <th>Jam Booking</th>
              <th>Status Booking</th>

                </tr>
            </tfoot>
        </table>
                        <!-- // Data table -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var table;
    $(document).ready(function() {
 
        //datatables
        table = $('#table').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                                "url": "@php
                echo base_url().'datajadwal'
                @endphp",
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
 
        });
 
    });
    </script>
@endsection