@extends('layout.members')

@section('title', 'JB Futsal | Booking')

@section('content')
  <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class='page-section padding-top-none'>
                        <div class='s-container'>
                            <h1 class='text-display-1 margin-top-none'>BOOKING LAPANGAN</h1>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                          @if(count($booking)>0)
                          <div class="card-book border-danger">
                            <div class="card-content">
                            <h4>Anda memiliki {{ count($booking) }} pesanan yang belum diproses!  <a href="{{ base_url() }}keranjang">Proses Sekarang</button></h4>
                            </div>
                          </div>
                          @endif
                        @foreach($datas as $data)
                        <div class="col-md-4 col-sm-6">
                            <div class="card-book col-md-12">
                              <div style="width: 100%; padding-top: 10px;">
                              <img src="{{ base_url().'assets/images/fields/'.$data['gambar'] }}" style="width: 100%;">
                              </div>
                              <div class="card-content">
                                <h2 class="text-primary pull-left">Lapangan No. {{ $data['kdLap'] }}</h2>
                                <div class="pull-right">
                                <a class="btn btn-outline-primary btn-sm border-primary" data-toggle="modal" data-target="#myModal" onclick="show_book('{{ base_url()}}','{{ $data['kdLap'] }}','{{$data['gambar']}}')">Pilih</a>
                                </div>
                              </div>
                          </div>
                        </div>
                        @endforeach
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
            <form action="#" enctype="multipart/form-data" method="POST" id="update_lap" data-url="{{base_url()}}">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Booking Lapangan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <div class="card border-primary flex-md-row mb-4 shadow-sm h-md-250">
                        <div>
                        <img class="card-img-right flex-auto d-none d-lg-block" alt="Thumbnail [200x250]" src="" style="width: 150px; height: 150px;" id="image">
                        </div>
                         <div class="card-body">
                           <strong class="d-inline-block mb-2 text-primary" id="no_lap"></strong>
                           <input type="hidden" name="kode" id="kode">
                           <p class="card-text mb-auto"></p>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Tgl Booking:</label>
                    <input type="text" class="form-control" name="tgl" id="tgl" placeholder="Pilih tgl booking">
                  </div>
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Jam Booking:</label>
                    <input type="text" class="form-control" name="jam" id="jam" placeholder="Pilih jam booking (jj:mm)">
                  </div>
                  <div id="after"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Lanjutkan</button>
              </div>
          </form>
            </div>
          </div>
        </div>
<script type="text/javascript">

    $( "#tgl" ).datepicker({
      dateFormat: "yy-mm-dd",
      minDate: 0,
    });


    function show_book(url, id, img) {
       $('#update_lap').attr('action', url+'booking/chart');
       $('#image').attr('src', url+'assets/images/fields/'+img);
       $('#no_lap').text('Lapangan No. '+id);
       $('#kode').val(id);
    }

    $("form").submit(function(e){
        e.preventDefault();
        _this         = $(this);
        urls          = _this.attr('data-url');
        serializeData = _this.serialize();
        $.ajax({
        url : urls+'booking/valid',
        type: "POST",
        dataType : 'json',
        data: serializeData,
        success: function(data){
            console.log(data.status);
            if (!data.status) {
              _this.submit(); 
            }else{
               $('#after').append('<label class="label label-danger">Jadwal tidak tersedia</label>');
            }
          },
        error: function (jqXHR, textStatus, errorThrown){
            console.log('Error get data from ajax');
        }
        });
    });
</script>
@endsection