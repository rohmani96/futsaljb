
@extends('layout.members')

@section('title', 'JB Futsal | Home')

{{-- @if($this->session->flashdata('error_msg') != null)

@error ($this->session->flashdata('error_msg'))

@endif --}}

@php
  $CI = &get_instance();
  if($CI->session->flashdata("msg")!=null){
  echo '<div class="alert alert-danger">'.$CI->session->flashdata("msg").'</div>';
}
@endphp

@section('content')

<div class="parallax cover overlay cover-image-full home">
    <img class="parallax-layer" src="assets/images/hom.jpg" alt="Learning Cover" />
    <div class="parallax-layer overlay overlay-full overlay-bg-white bg-transparent" data-speed="2" data-opacity="false">
        <div class="v-center">
            <div class="page-section overlay-bg-white-strong relative paper-shadow" data-z="1">
                <h1 class="text-display-2 margin-v-0-15">JB FUTSAL CIKARANG BARU</h1>
                @if (!isset($_SESSION['online']))
				    <a class="btn btn-blue-500 btn-md paper-shadow" data-hover-z="2" data-animated data-toggle="modal" href="#modal-overlay-signup">MENDAFTAR MEMBER</a>
				@endif
                    <a class="btn btn-green-500 btn-md paper-shadow" data-hover-z="2" data-animated data-toggle="modal" href="#modal-overlay-booking">CARA BOOKING</a>
            </div>
        </div>
    </div>
</div>

<div class="modal grow modal-overlay modal-backdrop-body fade" id="modal-overlay-signup">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <div class="modal-dialog">
        <div class="v-cell">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="wizard-container wizard-1" id="wizard-demo-1">
                        <div data-scrollable-h>
                            <ul class="wiz-progress">
                                <li class="active">Mulai Akun</li>
                                <li>Rincian</li>
                                <li>Informasi Kontak</li>
                            </ul>
                        </div>
                        <form action="{{ base_url().'signup' }}" method="POST" data-toggle="wizard" class="max-width-500 h-center" enctype="multipart/form-data">
                            <fieldset class="step relative paper-shadow form-horizontal" data-z="2">
                              <div class="page-section-heading">
                                    <h2 class="text-h3 margin-v-0">Mulai Membuat Akun Anda</h2>
                                    <h3 class="text-h4 margin-v-10 text-grey-400">Silahkan Isikan Klik Next Untuk Melanjutkan</h3>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="wiz-next btn btn-primary">Next</button>
                                </div>
                            </fieldset>
                            <fieldset class="step relative paper-shadow" data-z="2">
                                <div class="page-section-heading">
                                    <h2 class="text-h3 margin-v-0">LENGKAPI DATA AKUN ANDA</h2>
                                    <h3 class="text-h4 margin-v-10 text-grey-400">Klik Line Untuk Mengisi Data</h3>
                                </div>
                                    <div class="form-group">
                                        <label for="wiz-lusername">Username</label>
                                    <input class="form-control" type="text" id="wiz-lusername" name="txtUsername" />
                                    

                               
                                </div>
                                   <div class="form-group">
                                    <label for="wiz-lpass">Password</label>
                                    <input class="form-control" type="password" id="wiz-lpass" name="txtPass" />
                                    
                                    
                                </div>

                                <div class="form-group">
                                     <label for="wiz-email">Email</label>
                                    <input name="txtEmail" class="form-control" type="email" id="wiz-email" />
                                   
                                </div>
                                 
                                  
                                  
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="button" class="wiz-prev btn btn-default">Previous</button>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <button type="button" class="wiz-next btn btn-primary">Next</button>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="step relative paper-shadow" data-z="2">
                                <div class="page-section-heading">
                                    <h2 class="text-h3 margin-v-0">Informasi Kontak</h2>
                                    <h3 class="text-h4 margin-v-10 text-grey-400">Klik Line Untuk Mengisi Data</h3>
                                </div>
                                 <div class="form-group ">
                                     <label for="wiz-lname">Nama Lengkap</label>
                                    <input class="form-control" type="text" id="wiz-lname" name="txtNmLengkap" />
                                   
                                    
                                </div>  
                                 
                                <div class="form-group">
                                    <label for="wiz-address">Alamat</label>
                                    <textarea name="txtAlamat" rows="3" class="form-control" id="wiz-address" placeholder="Alamat"></textarea>
                                  
                                </div>             
                                
                                
                                <div class="form-group">
                                    <label for="wiz-nohp1">Handphone</label>
                                    <input name="txtKontak" class="form-control" type="text" id="wiz-nohp1" placeholder="Nomor HP/ Telpon" />
                                    
                                </div>
                                
                              
                                 <div class="form-group">
                                
                                    <label for="wiz-photo">Upload Foto:</label>
                                
                                    <input class="form-control" type="file" id="wiz-photo"  name="upPhoto" />
                                  
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="button" class="wiz-prev btn btn-default">Previous</button>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <button type="submit" class="btn btn-primary">Daftar</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal grow modal-overlay modal-backdrop-body fade" id="modal-overlay-booking">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <div class="modal-dialog">
        <div class="v-cell">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="wizard-container wizard-1" id="wizard-demo-1">
                        <fieldset class="step relative paper-shadow form-horizontal" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Untuk Mulai Booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Silahkan login terlebih dahulu</h3>
                                <img src="{{base_url()}}assets/images/demo/1.png" style="height: 200px;">
                            </div>
                        </fieldset>
                        <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Pilih menu Booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Pilih lapangan yang diinginkan</h3>
                                <img src="{{base_url()}}assets/images/demo/2.png" style="height: 200px;">
                            </div>
                        </fieldset>
                        <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Isi data booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Isi tanggal dan jam booking</h3>
                                <img src="{{base_url()}}assets/images/demo/3.png" style="height: 300px;">
                            </div>
                        </fieldset>
                         <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Keranjang booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Klik keranjang atau teks tersebut untuk melanjutkan booking</h3>
                                <img src="{{base_url()}}assets/images/demo/4.png" style="height: 150px;">
                            </div>
                        </fieldset>
                        <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Proses booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Isi data dan pilih booking yang telah diisi sebelumnya dan klik selesai</h3>
                                <img src="{{base_url()}}assets/images/demo/5.png" style="height: 200px;">
                            </div>
                        </fieldset>
                        <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Membayar booking</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Pilih menu inovice dan pilih selengkapnya</h3>
                                <img src="{{base_url()}}assets/images/demo/6.png" style="height: 200px;">
                            </div>
                        </fieldset>
                        <fieldset class="step relative paper-shadow" data-z="2">
                          <div class="page-section-heading">
                                <h2 class="text-h3 margin-v-0">Membayar booking 2</h2>
                                <h3 class="text-h4 margin-v-10 text-grey-400">Bayar booking ke rekening terter dan konfirmasi atau batalkan booking</h3>
                                <img src="{{base_url()}}assets/images/demo/7.png" style="height: 200px;">
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection