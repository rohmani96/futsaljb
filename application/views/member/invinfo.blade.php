@extends('layout.members')

@section('title', 'JB Futsal | Inovice Info')

{{-- @if($this->session->flashdata('error_msg') != null)

@error ($this->session->flashdata('error_msg'))

@endif --}}

@section('content')
<div class="container">
<div class="page-section">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class='page-section padding-top-none'>
                <div style="background-color: white;" id="tab-inv">
                  <table class="table table-bordered">
                	<tbody>
                  <tr>
                    <td colspan='6' align='center'>
                      <h4><strong>JB FUTSAL CIKARANG</strong></h4>
                      <strong>JL.SUPRAPTO - 0561 000 999</strong>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"><strong>RINCIAN BOOKING</strong></td>
                    <td colspan='2'><strong>RINCIAN IDENTITAS</strong></td>
                    <td colspan='2'><strong>RINCIAN INVOICE</strong></td>
                  </tr>
                  <tr>
                    <td>NO LAPANGAN</td>
                    <td>@foreach($inovice as $book){{$book['kdLap']}}, @endforeach</td>
                    <td>ATAS NAMA</td>
                    <td><strong>{{$datas[0]['an']}}</label></strong></td>
                    <td width='15%'>NO INVOICE</td>
                    <td colspan='2'><strong>INV/{{$datas[0]['kdInovice']}}</strong></td>
                  </tr>
                  <tr>
                    <td>TGL BOOKING</td>
                    <td>@foreach($inovice as $book){{$book['tglBook']}},  @endforeach</td>
                    <td>ALAMAT</td>
                    <td>{{$datas[0]['alamat']}}</td>
                    <td>STATUS</td>
                    <td colspan='2'>{{$datas[0]['statusBayar']}}</td>
                  </tr>
                  <tr>
                    <td rowspan="2">JAM BOOKING</td>
                    <td rowspan="2">@foreach($inovice as $book){{$book['jamBook'].'-'.$book['jamSelesai']}}<br> @endforeach</td>
                    <td>KONTAK</td>
                    <td>{{$datas[0]['kontak']}}</td>
                    <td>TGL. INVOICE</td>
                    <td colspan='2'>{{$datas[0]['tglPesan']}}</td>
                  </tr>
                  <tr>
                    
                    <td>EMAIL</td>
                    <td>{{$datas[0]['email']}}</td>
                    <td><strong>TOTAL BAYAR</strong></td>
                    <td colspan='2'><strong>Rp. {{$datas[0]['subTotal']}}</strong></td>
                  </tr>
                  <tr>
                    <td colspan="6"><strong>Note: Untuk melakukan booking lapangan kamu perlu melakukan DP minimal sebesar Rp. @php
                      echo $datas[0]['subTotal'] * 50/100;
                    @endphp</strong></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-5">Pembayaran melalui rekening berikut ini:<br>
                - BCA (071828282)
                - BRI (829283838)
                - BNI (83984490)</div>
            <div class="col-md-7">
                @if($datas[0]['statusBayar']=='belum')
                <button class='btn btn-success' data-toggle="modal" data-target="#myModal"><i class="fa fa-dollar"></i>&nbsp;Konfirmasi Pembayaran</button>
                <form action="{{base_url().'booking/batal'}}" method="POST" style="display: inline;" id="frm-hapus">
                @foreach($inovice as $book)
                <input type="hidden" name="kdbook" value="{{$datas[0]['kdBoking']}}">
                 <input type="hidden" name="kdinv" value="{{$book['kdInovice']}}">
                 <input type="hidden" name="kdjadwal[]" value="{{$book['kdJadwal']}}">
                @endforeach
                <button type="submit" class='btn btn-danger'><i class='fa fa-ban'></i>&nbsp;Batalkan Pemesanan</button>
                </form>
                 @else
                  <button class='btn btn-primary' onclick="printDiv()"><i class='fa fa-print'></i>&nbsp;Cetak Transaksi</button>
                 @endif
              </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="{{ base_url().'pembayaran' }}" enctype="multipart/form-data" method="POST">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Atas Nama:</label>
            <input type="text" class="form-control" name="nama">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">No Rekening:</label>
            <input type="text" class="form-control" name="norek">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Pembayaran:</label>
            <input type="text" class="form-control" name="jml" value="@php
                      echo $datas[0]['subTotal'] * 50/100;
                    @endphp" readonly="readonly">
          </div>
          <div class="form-group">
              <label for="exampleInputFile" class="col-form-label">Bukti Transfer:</label>
              <input type="file" name="filefoto" required>
    </div>
    <input type="hidden" name="kdinv" value="{{$datas[0]['kdInovice']}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Konfirmasi</button>
      </div>
  </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  function printDiv() {
        // var divToPrint=document.getElementById("tab-inv");
        //  newWin= window.open(""); 
        //  newWin.document.write(''.outerHTML);
        //  newWin.document.write(divToPrint.outerHTML);
        //  newWin.document.write('</body></html>'.outerHTML);
        //  newWin.print();
        //  newWin.close();
        var popupWin = window.open('', '_blank', 'width=auto,height=auto');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"></head><body>');
        popupWin.document.write($('#tab-inv').html());
        popupWin.document.write('</body></html>');
        popupWin.print();
        popupWin.document.close();
   }

   $("#frm-hapus").submit(function(e){
        e.preventDefault();
        _this         = $(this);
        if (confirm('Apakah Anda Yakin?')) {
          _this.submit();
        }else{
          e.preventDefault();
        }  
    });
   
</script>
@endsection