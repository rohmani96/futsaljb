<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title> @yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/dist/css/skins/skin-black.min.css">
    <link rel="stylesheet" href="{{ base_url() }}assets/jquery-ui/jquery-ui.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body class="hold-transition skin-black sidebar-mini">

<header class="main-header">
   <a href="{{base_url()}}admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>FJB</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Futsal</b> JB</span>
    </a>

  <!-- nav -->
  <nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account Menu -->
      <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <!-- The user image in the navbar-->
           @if (isset($_SESSION['foto']))
          <img src="{{ base_url().'assets/images/people/' . $_SESSION['foto'] }}" class="user-image" alt="User Image">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span class="hidden-xs">{{ $_SESSION['nama'] }}</span>
          @else
          <img src="{{ base_url().'assets/gambar/user.png' }}" class="user-image" alt="User Image">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span class="hidden-xs">{{ $_SESSION['nama'] }}</span>
          @endif
        </a>
        <ul class="dropdown-menu">
          <!-- The user image in the menu -->
          <li class="user-header">
            @if (isset($_SESSION['foto']))
            <img src="{{ base_url().'assets/images/people/' . $_SESSION['foto'] }}" class="img-circle" alt="User Image">

            <p>
              {{ $_SESSION['nama'] }}
            </p>
            @else
             <img src="{{ base_url().'assets/gambar/user.png' }}" class="img-circle" alt="User Image">

            <p>
              {{ $_SESSION['nama'] }}
            </p>
            @endif
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
              <a href="{{base_url()}}admin/signout" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">LIST MENU</li>
      <!-- Optionally, you can add icons to the links -->

      <li class="active">
        <a href="{{ base_url() }}admin/dashboard">
          <i class="fa fa-home"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      <li>
        <a href="{{ base_url() }}admin/pengguna">
          <i class="fa fa-user"></i>
          <span>Modul Pengguna</span>
        </a>
      </li>

      <li>
        <a href="{{ base_url() }}admin/booking">
          <i class="fa fa-briefcase"></i>
          <span>Modul Booking</span>
        </a>
      </li>
      
      <li>
        <a href="{{ base_url() }}admin/lapangan">
          <i class="fa fa-futbol-o "></i>
          <span>Modul Lapangan</span>
        </a>
      </li>

      <li>
        <a href="{{ base_url() }}admin/inovice">
          <i class="fa fa-address-card-o"></i>
          <span>Modul Inovice</span>
        </a>
      </li>

    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
  <!-- headerContent -->
  <!-- Main content -->
  <section class="content">
    @yield('content')
  </section>
</div>

<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Dashboard Admin
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">noone</a>.</strong> All rights reserved.
</footer>
<script src="{{ base_url() }}assets/jquery-ui/jquery-ui.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="{{ base_url() }}assets/dist/js/app.min.js"></script>

   
</body>
</html>