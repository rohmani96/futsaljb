<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';

$route['profile'] 			 = 'home/profile';

$route['signup'] 			 = 'users/signup';
$route['signin'] 			 = 'users/signin';
$route['signout'] 			 = 'users/signout';
$route['pengguna/(:any)'] 	 = 'users/profile/$1';
$route['user/save/admin'] 	 = 'users/save_admin';
$route['pengguna/update/(:any)'] 	 = 'users/update_user/$1';
$route['login_user'] 		 = 'users/login';

$route['lapangan'] 		 	 = 'field';
$route['lapangan/simpan'] 	 = 'field/save';
$route['lapangan/update/(:any)'] = 'field/update/$1';
$route['lapangan/hapus/(:any)']  = 'field/delete/$1';

$route['jadwal'] 	 		 = 'schedule';
$route['datajadwal'] 	 		 = 'schedule/get_all_jadwal';
$route['jadwal/simpan'] 	 = 'schedule/save';
$route['jadwal/update/(:any)'] = 'schedule/update/$1';
$route['jadwal/hapus/(:any)'] = 'schedule/delete/$1';


$route['booking'] 		 	 = 'booking';
$route['booking/batal'] = 'booking/batal_booking';
$route['booking/valid'] 	 = 'booking/validasi';
$route['booking/chart'] 	 = 'booking/save_chart';
$route['booking/inovice'] 	 = 'booking/save_booking';
$route['keranjang']  		 = 'booking/keranjang';
$route['keranjang/hapus/(:any)'] = 'booking/hapus_keranjang/$1';
$route['inovice'] 	 		 = 'booking/inovice';
$route['datainovice'] 	 		 = 'booking/data_inovice_user';
$route['inovice/detail/(:any)'] 	 = 'booking/inovice_detail/$1';
$route['inovice/bayar/(:any)'] 	 	 = 'booking/update_inovice/$1';
$route['pembayaran'] 	 	 = 'booking/add_payment';
$route['pembayaran/valid/(:any)']    = 'booking/update_valid/$1';
$route['pembayaran/hapus/(:any)']    = 'booking/hapus_pembayaran/$1';
$route['pembayaran/unprocced']    = 'booking/update_pembayaran';
$route['inovice/(:any)'] 	 = 'booking/get_inovice/$1';


$route['admin'] 	 		 = 'home/dashboard';
$route['admin/dashboard'] 	 = 'home/dashboard';
$route['admin/pengguna'] 	 = 'users';
$route['admin/pengguna/(:any)']  = 'users';
$route['admin/booking'] 	     = 'schedule/admin';
$route['admin/databooking']  = 'schedule/get_booking';
$route['admin/lapangan'] 	 = 'field/admin';
$route['admin/inovice'] 	 = 'booking/admin_inovice';
$route['admin/datainovice']  = 'booking/data_inovice';
$route['admin/signout'] 	 = 'users/signout';

$route['404_override'] 		 = '';
$route['translate_uri_dashes'] = FALSE;
