<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_booking');
		$this->load->model('m_users');
		$this->load->model('m_schedule');
	}

	public function index()
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');

		if ($session != '') {
			$book = $this->m_booking->get_keranjang($id);
			return view('member.home' ,['booking'=> $book, 'menu'=>'home']);
		}else{
			return view('member.home', ['menu'=>'home']);
		}
	}

	public function profile()
	{
		return view('member.profile', ['menu'=>'profile']);
	}

	public function dashboard()
	{
		$session = $this->session->userdata('online');
		$role 	 = $this->session->userdata('role');

		if ($session == '' || $role==1 ) {
			redirect('/');
		} else {
			$count = count($this->m_users->get_all_user());
			$jadwal = count($this->m_schedule->get_count_jadwal());
			$inv  = count($this->m_booking->get_count_inovice());
			$book = count($this->m_booking->get_count_booking());
			$data = $this->m_booking->get_payment();
			return view('admin.dashboard', ['user'=>$count, 'jadwal'=>$jadwal, 'inovice'=> $inv, 'booking'=>$book, 'datas'=> $data]);
		}
	}
}
