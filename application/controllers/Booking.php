<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_booking');
		$this->load->library('upload');
	}

	public function index()
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');

		if ($session == '') {
			redirect('/');
		} else {
			$data = $this->m_booking->get_jadwal();
			$book = $this->m_booking->get_keranjang($id);
			return view('member.booking' ,['datas' => $data, 'booking'=> $book, 'menu'=>'booking']);
		}
	}

	public function admin_inovice()
	{
		$session = $this->session->userdata('online');
		$role = $this->session->userdata('role');

		if ($session == ''|| $role!=0) {
			redirect('/');
		} else {
			return view('admin.inovice');
		}
	}

	public function data_inovice()
	{
		$list = $this->m_booking->get_datatables();
	        $data = array();
	        $no = $_POST['start'];
	        foreach ($list as $field) {
	            $row = array();
	            $row[] = 'INV/'.$field->kdInovice;
	            $row[] = $field->tglPesan;
	            $row[] = $field->tglBayar;
	            $row[] = $field->an;
	            $row[] = 'Rp.'.$field->subTotal;
	            if ($field->statusBayar=='proses'||$field->statusBayar=='valid') {
	            	$row[] = "<label class='alert alert-success'>$field->statusBayar</label>";
	            	$row[]= "<button class='btn btn-info'  data-toggle='modal' data-target='#myModal' data-url='".base_url()."' data-id='$field->kdInovice'><i class='fa fa-info'></i></button>";
	            }else if ($field->statusBayar=='lunas') {
	            	$row[] = "<label class='alert alert-primary'>$field->statusBayar</label>";
	            	$row[]= "<a href='".base_url()."inovice/bayar/$field->kdInovice' class='btn btn-danger'><i class='fa fa-ban'></i></a>&nbsp;<button class='btn btn-info'  data-toggle='modal' data-target='#myModal' data-url='".base_url()."' data-id='$field->kdInovice'><i class='fa fa-info'></i></button> <a href='".base_url()."inovice/$field->kdInovice' class='btn btn-info' style='margin-bottom: 10px;' target='_blank'><i class='fa fa-print'></i></a>";
	            }else{
	            	$row[] = "<label class='alert alert-danger'>$field->statusBayar</label>";
	            	$row[]= "<button class='btn btn-primary' data-toggle='modal' data-target='#payModal' data-id='$field->kdInovice' data-jml='$field->subTotal' data-url='".base_url()."''><i class='fa fa-check-square-o'></i></button>
                <button class='btn btn-info'  data-toggle='modal' data-target='#myModal' data-url='".base_url()."' data-id='$field->kdInovice'><i class='fa fa-info'></i></button>";
	            }

	            $data[] = $row;
	        }

	        $output = array(
	            "draw" => $_POST['draw'],
	            "recordsTotal" => $this->m_booking->count_all(),
	            "recordsFiltered" => $this->m_booking->count_filtered(),
	            "data" => $data,
	        );
	        //output dalam format JSON
	        echo json_encode($output);
	}

	public function validasi()
	{
		$id = $this->session->userdata('iduser');
		$data = $this->m_booking->cek_semua()->result_array();
		$data2 = $this->m_booking->cek_user($id)->result_array();
		if (count($data)>0 || count($data2)>0) {
			$status = true;
		}else{
			$status = false;
		}

		$arr = array(
		  'status' => $status,
		);

		echo json_encode($arr);;
	}

	public function save_chart()
	{
		$this->m_booking->save_chart();
		redirect('booking');
	}


	public function keranjang()
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');

		if ($session == '') {
			redirect('/');
		} else {
			$book = $this->m_booking->get_booking($id);
			$data = $this->m_booking->get_keranjang($id);
			$user = $this->m_booking->get_profile($id);
			
			return view('member.keranjang' ,['booking' => $data, 'datas' => $data, 'profile' => $user, 'menu'=>'booking']);
		}
	}

	public function hapus_keranjang($id)
	{
		$this->m_booking->hapus_keranjang($id);
		redirect('keranjang');
	}

	public function save_booking()
	{
		$id = $this->session->userdata('iduser');
		$kode = $this->m_booking->save_booking($id);
		redirect('inovice/'.$kode);
	}

	public function inovice()
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');

		if ($session == '') {
			redirect('/');
		} else {
			$book = $this->m_booking->get_keranjang($id);
			return view('member.inovice' ,['booking'=> $book, 'menu'=>'inovice']);
		}
	}


	public function data_inovice_user()
	{
		$id = $this->session->userdata('iduser');

		$list = $this->m_booking->get_datatables_inovice($id);
	        $data = array();
	        $no = $_POST['start'];
	        foreach ($list as $field) {
	            $row = array();
	            $row[] = 'INV/'.$field->kdInovice;
	            $row[] = $field->tglPesan;
	            $row[] = $field->tglBayar;
	            $row[] = $field->an;
	            $row[] = 'Rp.'.$field->subTotal;
	            $row[] = $field->statusBayar;
	            $row[] = "<a href='".base_url()."inovice/$field->kdInovice' class='btn btn-info' style='margin-bottom: 10px;'>Selengkapnya</a>";
	            
	            $data[] = $row;
	        }

	        $output = array(
	            "draw" => $_POST['draw'],
	            "recordsTotal" => $this->m_booking->count_all_inovice($id),
	            "recordsFiltered" => $this->m_booking->count_filtered($id),
	            "data" => $data,
	        );
	        //output dalam format JSON
	        echo json_encode($output);
	}

	public function get_inovice($kode)
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');
		if ($session == '') {
			redirect('/');
		} else {
			$book = $this->m_booking->get_keranjang($id);
			$data = $this->m_booking->get_inovice($kode);
			$inv = $this->m_booking->get_inv_book($kode);
			
			return view('member.invinfo' ,['booking'=> $book, 'datas'=>$data, 'inovice'=>$inv, 'menu'=>'inovice']);
		}
	}

	public function add_payment()
	{
		$config['upload_path'] = './assets/images/payment/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if ($this->upload->do_upload('filefoto'))
        {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/payment/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['new_image']= './assets/images/payment/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

				$this->m_booking->save_payment($gbr['file_name']);
				$this->session->set_flashdata('msg', 'Berhasil menyimpan data');
           		redirect('inovice');
		}else{
		   $this->session->set_flashdata('msg', $this->upload->display_errors());
           redirect('inovice');
        }
	}

	public function update_valid($id)
	{
		if ($this->input->post('kdInv')!==null) {
			$this->m_booking->update_valid($id, $this->input->post('kdInv'));
		}else{
			$this->m_booking->update_invalid($id);
		}
		
		redirect('admin');
	}

	public function update_inovice($id)
	{
		
		$this->m_booking->update_inovice($id);
		
		redirect('admin/inovice');
	}

	public function inovice_detail($id)
	{
		$data = $this->m_booking->get_inovice_detail($id);

		return $this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function hapus_pembayaran($id)
	{
		$this->m_booking->hapus_bayar($id);
		
		redirect('admin/inovice');
	}

	public function update_pembayaran()
	{
		$this->m_booking->update_bayar();
		
		redirect('admin/inovice');
	}

	public function batal_booking()
	{
		$this->m_booking->batal_booking($this->input->post('kdjadwal'), $this->input->post('kdinv'), $this->input->post('kdbook'));
		
		redirect('admin/inovice');
	}

}
