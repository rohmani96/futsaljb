<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_users');
		$this->load->model('m_booking');
		$this->load->library('upload');
		$this->load->library('pagination');
	}

	public function index($page=NULL)
	{
		$session = $this->session->userdata('online');
		$role = $this->session->userdata('role');
		$users = $this->m_users->get_all_user();

		if ($session == ''|| $role!=0) {
			redirect('/');
		} else {
			$page=$this->uri->segment(3);
	        if(!$page):
	            $offset = 0;
	        else:
	            $offset = $page;
	        endif;
	        $limit=20;
	        $config['base_url'] = base_url() . 'admin/pengguna/';
	        $config['total_rows'] = count($users);
	        $config['per_page'] = $limit;
	        $config['uri_segment'] = 3;
	        $config['first_link']       = 'First';
	        $config['last_link']        = 'Last';
	        $config['next_link']        = 'Next';
	        $config['prev_link']        = 'Prev';
	        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
	        $config['full_tag_close']   = '</ul></nav></div>';
	        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	        $config['num_tag_close']    = '</span></li>';
	        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
	        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
	        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['prev_tagl_close']  = '</span>Next</li>';
	        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	        $config['first_tagl_close'] = '</span></li>';
	        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['last_tagl_close']  = '</span></li>';
	        $this->pagination->initialize($config);

			$data = $this->m_users->get_user_pagination($offset, $limit);
			$pages = $this->pagination->create_links();
			
			return view('admin.user',  ['datas' => $data, 'page'=>$pages]);
		}
	}

	public function profile($id)
	{
		$session = $this->session->userdata('online');
		$book = $this->m_booking->get_keranjang($id);
		$data = $this->m_booking->get_profile($id);
		$history = $this->m_booking->history_booking($id);
		
		if ($session == ''|| $data[0]['kdPengguna']!=$id) {
			redirect('/');
		} else {
			return view('member.user',  ['menu'=>'kosong', 'booking'=> $book, 'datas' => $data, 'history'=>$history]);
		}
	}

	public function update_user($id)
	{
		$config['upload_path'] = './assets/images/people/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['upPhoto']['name']))
	        {
	        if ($this->upload->do_upload('upPhoto'))
	        {
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='./assets/images/people/'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '60%';
	                $config['width']= 480;
	                $config['height']= 480;
	                $config['new_image']= './assets/images/people/'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

					$this->m_users->update_user_gambar($id, $gbr['file_name']);
					$this->session->set_flashdata('msg', 'Berhasil update');
					// $session_data = array(
					// 'nama' 	 => $data-,
					// 'foto' 	 => $data->foto,
					// );
					// $this->session->set_userdata($session_data);
	           		redirect('pengguna/'.$id);
			}else{
			   $this->session->set_flashdata('msg', $this->upload->display_errors());
	           redirect('pengguna/'.$id);
	        }
	    }else{
	    	$this->m_users->update_user($id);
	    	$this->session->set_flashdata('msg', 'Berhasil update data');
	        redirect('pengguna/'.$id);
	    }
	}

	public function signin()
	{
		return view('member.login', ['menu'=>'kosong']);
	}

	public function signout()
	{
		$this->session->sess_destroy();
		redirect('signin');
	}

	public function signup()
	{
		$config['upload_path'] = './assets/images/people/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['upPhoto']['name']))
	        {
	        if ($this->upload->do_upload('upPhoto'))
	        {
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='./assets/images/people/'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '60%';
	                $config['width']= 480;
	                $config['height']= 480;
	                $config['new_image']= './assets/images/people/'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

					$this->m_users->register($gbr['file_name']);

					redirect('/');
			}else{
			   $this->session->set_flashdata('msg', $this->upload->display_errors());
	           redirect('/');
	        }
	    }else{
	    	$this->m_users->register();
	    }

	    redirect('/');
	   
	}

	public function save_admin()
	{
		
		$this->m_users->save_admin();

		redirect('admin/pengguna');
	}

	public function login()
	{
		$data = $this->m_users->login();

		if ($data==false) {
			redirect('signin');
		}else{
			$session_data = array(
				'iduser' => $data->kdPengguna,
				'nama' 	 => $data->nmPengguna,
				'foto' 	 => $data->foto,
				'role'	 => $data->role,
				'online' => "ya"
			);

			$this->session->set_userdata($session_data);
			if ($data->role==0) {
				redirect('admin');
			}else{
				redirect('/');
			}

		}
	}

}
