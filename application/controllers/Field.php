<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_lapangan');
		$this->load->library('upload');
	}

	public function index()
	{
		$data = $this->m_lapangan->get_all();
		return view('member.lapangan',['datas' => $data, 'menu'=>'lapangan']);
	}

	public function admin()
	{
		$session = $this->session->userdata('online');
		$role = $this->session->userdata('role');

		if ($session == ''|| $role!=0) {
			redirect('/');
		} else {
		$data = $this->m_lapangan->get_all();
		return view('admin.lapangan', ['datas' => $data]);
		}
	}

	public function save()
	{
		$config['upload_path'] = './assets/images/fields/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if ($this->upload->do_upload('filefoto'))
        {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/fields/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '60%';
                $config['width']= 710;
                $config['height']= 455;
                $config['new_image']= './assets/images/fields/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

				$this->m_lapangan->save($gbr['file_name']);
				$this->session->set_flashdata('msg', 'Berhasil menyimpan data');
           		redirect('admin/lapangan');
		}else{
		   $this->session->set_flashdata('msg', $this->upload->display_errors());
           redirect('admin/lapangan');
        }
	}

	public function update($id)
	{
		$config['upload_path'] = './assets/images/fields/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
	        {
	        if ($this->upload->do_upload('filefoto'))
	        {
	                $gbr = $this->upload->data();
	                //Compress Image
	                $config['image_library']='gd2';
	                $config['source_image']='./assets/images/fields/'.$gbr['file_name'];
	                $config['create_thumb']= FALSE;
	                $config['maintain_ratio']= FALSE;
	                $config['quality']= '60%';
	                $config['width']= 710;
	                $config['height']= 455;
	                $config['new_image']= './assets/images/fields/'.$gbr['file_name'];
	                $this->load->library('image_lib', $config);
	                $this->image_lib->resize();

					$this->m_lapangan->update_gambar($id, $gbr['file_name']);
					$this->session->set_flashdata('msg', 'Berhasil update data');
	           		redirect('admin/lapangan');
			}else{
			   $this->session->set_flashdata('msg', $this->upload->display_errors());
	           redirect('admin/lapangan');
	        }
	    }else{
	    	$this->m_lapangan->update($id);
	    	$this->session->set_flashdata('msg', 'Berhasil update data');
	        redirect('admin/lapangan');
	    }
	}

	public function delete($id)
	{
		$this->m_lapangan->delete($id);
		$this->session->set_flashdata('msg', 'Berhasil hapus data');
		redirect('admin/lapangan');        
	}

}
