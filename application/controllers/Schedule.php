<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_schedule');
		$this->load->model('m_booking');
	}

	public function index()
	{
		$session = $this->session->userdata('online');
		$id = $this->session->userdata('iduser');

		if ($session != '') {
			$book = $this->m_booking->get_keranjang($id);
			return view('member.jadwal' ,['booking'=> $book, 'menu'=>'jadwal']);
		} else {
			return view('member.jadwal' ,['menu'=>'home']);
		}

	}

	public function admin()
	{
		$session = $this->session->userdata('online');
		$role = $this->session->userdata('role');

		if ($session == ''|| $role!=0) {
			redirect('/');
		} else {
		return view('admin.jadwal');
	}
	}

	public function get_all_jadwal()
	{
		$list = $this->m_schedule->get_datatables('booked');
	        $data = array();
	        $no = $_POST['start'];
	        foreach ($list as $field) {
	            $no++;
	            $row = array();
	            $row[] = $no;
	            $row[] = $field->tglBook;
	            $row[] = 'No Lapangan '.$field->kdLap;
	            $row[] = $field->jamBook .'-'. $field->jamSelesai;
	            $row[] = $field->status;

	            $data[] = $row;
	        }
	        $output = array(
	            "draw" => $_POST['draw'],
	            "recordsTotal" => $this->m_schedule->count_all('booked'),
	            "recordsFiltered" => $this->m_schedule->count_filtered('booked'),
	            "data" => $data,
	        );
	        //output dalam format JSON
	        echo json_encode($output);
	}

	public function get_booking()
	{
		
			$list = $this->m_schedule->get_datatables();
	        $data = array();
	        $no = $_POST['start'];
	        foreach ($list as $field) {
	            $no++;
	            $row = array();
	            $row[] = $no;
	            $row[] = $field->tglBook;
	            $row[] = 'No Lapangan '.$field->kdLap;
	            $row[] = $field->jamBook .'-'. $field->jamSelesai;
	            $row[] = $field->status;

	            $data[] = $row;
	        }
	        $output = array(
	            "draw" => $_POST['draw'],
	            "recordsTotal" => $this->m_schedule->count_all(),
	            "recordsFiltered" => $this->m_schedule->count_filtered(),
	            "data" => $data,
	        );
	        //output dalam format JSON
	        echo json_encode($output);
	}

	public function save()
	{
		$this->m_schedule->save_jadwal();
		redirect('admin/jadwal');
	}

	public function update($id)
	{
		$this->m_schedule->update_jadwal($id);
		redirect('admin/jadwal');
	}

	public function delete($id)
	{
		$this->m_schedule->delete_jadwal($id);
		redirect('admin/jadwal');
	}

}
