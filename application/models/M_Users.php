<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Users extends CI_Model {

	public function login() {

		$uname 	   = $this->input->post('txtUsername');
		$password  = $this->input->post('txtPassword');

		$query = $this->db->get_where('tpengguna', array('username' => $uname));
		
		if ($query->num_rows() == 1) {
			$user  = $query->row();
			if (password_verify($password, $user->password)) {
			  return $user;
			}else{
			  $this->session->set_flashdata('msg', 'Password Anda Salah.');
			  return false;
			}
		} else {
			$this->session->set_flashdata('msg', 'Username tidak ditemukan.');
			return false;
		}
	}

	public function register($foto=NULL) {
		if($foto==NULL){
			$foto     = 'profile.png';
		}
		$password = password_hash($this->input->post('txtPass'), PASSWORD_DEFAULT);
		$nama     = $this->input->post('txtNmLengkap');
		$uname    = $this->input->post('txtUsername');

		$query = $this->db->get_where('tpengguna', array('username' => $uname));
		
		if ($query->num_rows() <= 0) {

			$data = array (
				'username' 		=> $this->input->post('txtUsername'),
				'password'  	=> $password,
				'nmPengguna'  	=> $nama,
				'emailPengguna' => $this->input->post('txtEmail'),
				'alamatPengguna'=> $this->input->post('txtAlamat'),
				'kontak' 		=> $this->input->post('txtKontak'),
				'foto'  		=> $foto,
				'role' 		    => 1
			);



			$this->db->insert('tpengguna', $data);

			$userid = $this->db->insert_id();

			 $session_data = array(
					'iduser' => $userid,
					'nama' 	 => $nama,
					'foto' 	 => $foto,
					'role'	 => 1,
					'online' => "ya"
				);

			$this->session->set_userdata($session_data);
		}else{
			$this->session->set_flashdata('msg', 'Username tidak tersedia');
		}

	}

	public function save_admin() {

		$foto     = 'profile.png';
		$password = password_hash($this->input->post('txtPass'), PASSWORD_DEFAULT);

		$data = array (
			'username' 		=> $this->input->post('txtUsername'),
			'password'  	=> $password,
			'nmPengguna'  	=> $this->input->post('txtNmLengkap'),
			'emailPengguna' => $this->input->post('txtEmail'),
			'alamatPengguna'=> $this->input->post('txtAlamat'),
			'kontak' 		=> $this->input->post('txtKontak'),
			'foto'  		=> $foto,
			'role' 		    => 0
		);

		return $this->db->insert('tpengguna', $data);
	}

	public function update_user($id) {

		$data = array (
			'nmPengguna'  	=> $this->input->post('txtNmLengkap'),
			'emailPengguna' => $this->input->post('txtEmail'),
			'alamatPengguna'=> $this->input->post('txtAlamat'),
			'kontak' 		=> $this->input->post('txtKontak')
		);

		$this->db->where('kdPengguna', $id);
		return $this->db->update('tpengguna', $data);
	}

	public function update_user_gambar($id, $url) {

		$data = array (
			'nmPengguna'  	=> $this->input->post('txtNmLengkap'),
			'emailPengguna' => $this->input->post('txtEmail'),
			'alamatPengguna'=> $this->input->post('txtAlamat'),
			'kontak' 		=> $this->input->post('txtKontak'),
			'foto'  		=> $url,
		);

		$this->db->where('kdPengguna', $id);
		return $this->db->update('tpengguna', $data);
	}


	public function get_all_user()
	{
		$query = $this->db->get('tpengguna');
		return $query->result_array();
	}

	public function get_user_pagination($off, $limit)
	{
		$query = $this->db->query("SELECT * FROM tpengguna limit $off, $limit");

		return $query->result_array();
	}

	// public function get_user($id)
	// {
	// 	$query = $this->db->get_where('tpengguna', array('kdPengguna' => $id));
	// 	$this->db->get('tpengguna');
	// 	return $query->result_array();
	// }
	
}