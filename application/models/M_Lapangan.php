<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Lapangan extends CI_Model {

	public function save($url) {

		$data = array (
			'kdLap' 	=> $this->input->post('nomor'),
			'harga' 	=> $this->input->post('harga'),
			'deskripsi' => $this->input->post('desc'),
			'gambar'=> $url
		);

		return $this->db->insert('tlapangan', $data);
	}

	public function update($id) {

		$data = array (
			'kdLap' 	=> $this->input->post('nomor'),
			'harga' 	=> $this->input->post('harga'),
			'deskripsi' => $this->input->post('desc')
		);

		$this->db->where('kdLap', $id);
		return $this->db->update('tlapangan', $data);
	}

	public function update_gambar($id, $url) {

		$data = array (
			'kdLap' 	=> $this->input->post('nomor'),
			'harga' 	=> $this->input->post('harga'),
			'deskripsi' => $this->input->post('desc'),
			'gambar'	=> $url
		);

		$this->db->where('kdLap', $id);
		return $this->db->update('tlapangan', $data);
	}

	public function get_all()
	{
		$query = $this->db->get('tlapangan');
		return $query->result_array();
	}

	public function delete($id)
	{
		return $this->db->delete('tlapangan', array('kdLap'=>$id));
	}
	
}