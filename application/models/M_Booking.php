<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Booking extends CI_Model {

	var $table = 'tinovice'; //nama tabel dari database
    var $column_order = array(null, 'kdInovice', 'tglPesan', 'tglBayar', 'an', 'subTotal','statusBayar'); //field yang ada di table user
    var $column_search = array('tglPesan', 'an'); //field yang diizin untuk pencarian 
    var $order = array('tglPesan' => 'desc'); // default order 


	public function get_jadwal()
	{
		// $this->db->select('*');
		// $this->db->from('tjadwal');
		// $this->db->join('tlapangan', 'tlapangan.kdLapangan = tjadwal.kdLapangan');
		// $query = $this->db->get();
		$query = $this->db->get('tlapangan');
		return $query->result_array();
	}

	public function get_booking($id)
	{
		$this->db->select('*');
		$this->db->from('tboking_temp');
		$this->db->join('tjadwal', 'tjadwal.kdJadwal = tboking_temp.kdJadwal');
		$this->db->where('idSession', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_keranjang($id)
	{
		$this->db->select('*');
		$this->db->from('tboking_temp');
		$this->db->join('tlapangan', 'tlapangan.kdLap = tboking_temp.kdLap');
		$this->db->join('tjadwal', 'tjadwal.kdJadwal = tboking_temp.kdJadwal');
		$this->db->where('idSession', $id);
		$this->db->where('status', 'unbook');
		$query = $this->db->get();
		return $query->result_array();
	}


	public function get_profile($id)
	{
		$this->db->select('*');
		$this->db->from('tpengguna');
		$this->db->where('kdPengguna', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_harga($ids)
	{
		$this->db->select_sum('harga');
		$this->db->from('tjadwal');
		$this->db->join('tlapangan', 'tlapangan.kdLap = tjadwal.kdLap');
		$this->db->where_in('kdJadwal', $ids);
		$query = $this->db->get();
		return $query->result_array();
	}

	//inovice

	public function get_all_inovice($id)
	{
		$this->db->select('*');
		$this->db->from('tinovice');
		$this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
		// $this->db->join('tjadwal', 'tjadwal.kdInovice = tinovice.kdInovice');
		$this->db->where('kdUser', $id);
		$query = $this->db->get();
		return $query->result_array();
	}


    private function _get_datatables_inovice($id)
    {
         
        $this->db->from('tinovice');
        $this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
        $this->db->where('kdUser', $id);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        	 {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_inovice($id)
    {
        $this->_get_datatables_inovice($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered_inovice($id)
    {
        $this->_get_datatables_inovice($id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_inovice($id)
    {
        $this->db->from('tinovice');
         $this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
        $this->db->where('kdUser', $id);
        return $this->db->count_all_results();
    }


	public function get_inovice($kode)
	{
		$this->db->select('*');
		$this->db->from('tinovice');
		$this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
		$this->db->where('kdInovice', $kode);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_count_inovice()
	{
		$query = $this->db->get_where('tinovice', array('statusBayar' => 'belum'));
		return $query->result_array();
	}

	public function get_count_booking()
	{
		$query = $this->db->get('tboking_temp');
		return $query->result_array();
	}



	public function get_inv_book($kode)
	{
		$this->db->select('*');
		$this->db->from('tboking');
		$this->db->join('tjadwal', 'tjadwal.kdJadwal = tboking.kdJadwal');
		$this->db->where('kdInovice', $kode);
		$query = $this->db->get();
		return $query->result_array();
	}



	public function cek_semua()
	{
		$kode       = $this->input->post('kode');
		$tgl        = $this->input->post('tgl');
		$jam        = $this->input->post('jam');
		$timestamp_m= strtotime($jam) +60;
		$jamMulai   = date('H:i', $timestamp_m);
		$timestamp_s= strtotime($jam) + 60*60;
		$jamSelesai = date('H:i', $timestamp_s);

        $query =$this->db->query("SELECT * FROM `tjadwal` WHERE kdLap='$kode' and tglBook='$tgl' and status='booked' and jamSelesai BETWEEN '$jamMulai' AND '$jamSelesai'");
        return $query;
	}

	public function cek_user($id)
	{
		$kode       = $this->input->post('kode');
		$tgl        = $this->input->post('tgl');
		$jam        = $this->input->post('jam');
		$timestamp_m= strtotime($jam) +60;
		$jamMulai   = date('H:i', $timestamp_m);
		$timestamp_s= strtotime($jam) + 60*60;
		$jamSelesai = date('H:i', $timestamp_s);

        $query =$this->db->query("SELECT * FROM `tjadwal` INNER JOIN tboking_temp ON tboking_temp.kdJadwal = tjadwal.kdJadwal WHERE tjadwal.kdLap='$kode' and tglBook='$tgl' and tboking_temp.idSession=$id and jamSelesai BETWEEN '$jamMulai' AND '$jamSelesai'");
        return $query;
	}

	public function save_chart()
	{
		$kdJadwal   = uniqid();
		$kode       = $this->input->post('kode');
		$tgl        = $this->input->post('tgl');
		$jamMulai   = $this->input->post('jam');
		$timestamp  = strtotime($jamMulai) + 60*60;
		$jamSelesai = date('H:i', $timestamp);
		$idUser		= $this->session->userdata('iduser');

		$jadwal = array (
			'kdJadwal' 	=> $kdJadwal,
			'kdLap' 	=> $kode ,
			'jamBook' 	=> $jamMulai,
			'jamSelesai'=> $jamSelesai,
			'tglBook' 	=> $tgl,
			'status' 	=> 'unbook'
		);

		$booking = array(
			'kdLap' 	=> $kode ,
			'kdJadwal' 	=> $kdJadwal,
			'idSession' => $idUser
		);

		$this->db->insert('tjadwal', $jadwal);
		$this->db->insert('tboking_temp', $booking);
	}

	public function save_booking($iduser)
	{
		$nama  = $this->input->post('nama');
		$email = $this->input->post('email');
		$kontak= $this->input->post('kontak');
		$alamat= $this->input->post('alamat');
		$idbook= $this->input->post('idbook');

		$harga = $this->get_harga($idbook);

		$data = array (
			'an' 	=> $nama,
			'alamat'=> $alamat,
			'email'	=> $email,
			'kontak'=> $kontak
		);

		$this->db->insert('tboking_info', $data);

		$inovice = array (
			'kdBoking'=> $this->db->insert_id(),
			'kdUser'  => $iduser,
			'subTotal'=> $harga[0]['harga'],
			'statusBayar'=> 'belum'
		);

		$this->db->insert('tinovice', $inovice);

		$kdInv = $this->db->insert_id();

		$this->save_related($kdInv, $idbook);
		$this->update_jadwal($idbook, 'booking');

		return $kdInv;
	}

	public function save_related($kdInv, $kdJadwal)
	{
		$data = array(); 
		foreach ($kdJadwal as $id) {
		   array_push($data, array(
		  	'kdInovice'=> $kdInv,
			'kdJadwal'  => $id,
		   ));
		}   

		$this->db->insert_batch('tboking', $data); 

	}

	public function update_jadwal($kode, $kondisi) {

		$data = array(); 
		foreach ($kode as $id) {
		   array_push($data, array(
		  'kdJadwal' => $id ,
		  'status' => $kondisi
		   ));
		}   

		$this->db->update_batch('tjadwal', $data, 'kdJadwal'); 
	}

	public function update_jadwal_fromdb($kode, $kondisi) {

		$data = array(); 
		foreach ($kode as $id) {
		   array_push($data, array(
		  'kdJadwal' => $id['kdJadwal'] ,
		  'status' => $kondisi
		   ));
		}   

		$this->db->update_batch('tjadwal', $data, 'kdJadwal'); 
	}

	public function history_booking($id)
	{
		$this->db->select('*');
		$this->db->from('tboking_temp');
		$this->db->join('tlapangan', 'tlapangan.kdLap = tboking_temp.kdLap');
		$this->db->join('tjadwal', 'tjadwal.kdJadwal = tboking_temp.kdJadwal');
		$this->db->where('idSession', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function save_payment($url)
	{
		$kdInv = $this->input->post('kdinv');
		$data = array (
			'atasnama' 	=> $this->input->post('nama'),
			'norek' 	=> $this->input->post('norek'),
			'jmlBayar'  => $this->input->post('jml'),
			'status' 	=> 'unprocced',
			'gambar'	=> $url,
			'kdInv'		=> $kdInv
		);

		$this->db->insert('inovice_info', $data);

		$data2 = array (
		'statusBayar' 	=> 'proses'
		);

		$this->db->where('kdInovice',$kdInv);
		$this->db->update('tinovice', $data2);
	}

	public function get_payment()
	{
		$this->db->select('*');
		$this->db->from('inovice_info');
		$this->db->where('status', 'unprocced');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_kode_jadwal($kode)
	{
		$this->db->select('kdJadwal');
		$this->db->from('tboking');
		$this->db->where('kdInovice', $kode);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_valid($id, $kode)
	{
		$data = array (
				'status' 	=> 'procced'
			);

	$this->db->where('id', $id);
	$this->db->update('inovice_info', $data);

	$data2 = array (
		'tglBayar' 	=>  date("Y-m-d H:i:s"),
		'statusBayar' 	=> 'valid'
	);

	$this->db->where('kdInovice',$kode);
	$this->db->update('tinovice', $data2);

	$kdJadwal = $this->get_kode_jadwal($kode);

	$this->update_jadwal_fromdb($kdJadwal, 'booked');

	}

	public function update_invalid($id)
	{
		$data = array (
				'status' 	=> 'unprocced'
			);

	$this->db->where('id', $id);
	 $this->db->update('inovice_info', $data);
	}

	public function update_inovice($id)
	{

		if ($this->input->post('anama')!==null) {
			$data = array (
				'atasnama' 	=> $this->input->post('anama'),
				'jmlBayar'  => $this->input->post('jml'),
				'status' 	=> 'procced',
				'kdInv'		=> $id
			);

			$this->db->insert('inovice_info', $data);

			$data2 = array (
			'tglBayar' 		=>  date("Y-m-d H:i:s"),
			'statusBayar' 	=> 'lunas'
			);

			$this->db->where('kdInovice',$id);
			$this->db->update('tinovice', $data2);
		}else{
			$data2 = array (
			'tglBayar' 		=> '0000-00-00 00:00:00',
			'statusBayar' 	=> 'valid'
			);

			$this->db->where('kdInovice',$id);
			$this->db->update('tinovice', $data2);
		}
	}
//inovice
	
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
        $this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking'); 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        	 {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        $this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
        return $this->db->count_all_results();
    }

	public function get_transaksi()
	{
		
		$this->db->select('*');
		$this->db->from('tinovice');
		$this->db->join('tboking_info', 'tboking_info.kdBoking = tinovice.kdBoking');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_inovice_detail($id)
	{
		$this->db->select('*');
		$this->db->from('tinovice');
		$this->db->join('inovice_info', 'inovice_info.kdInv = tinovice.kdInovice');
		$this->db->where('kdInovice', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function hapus_bayar($id)
	{
		return $this->db->delete('inovice_info', array('id'=>$id));
	}

	public function update_bayar()
	{
		$data = array (
				'status' 	=> 'unprocced'
			);

		$kode = $this->input->post('kdinv');
		$id = $this->input->post('id');


		$this->db->where('id', $id);
	 	$this->db->update('inovice_info', $data);

	 	$data2 = array (
		'tglBayar' 	=>  '0000-00-00 00:00',
		'statusBayar' 	=> 'proses'
		);

		$this->db->where('kdInovice',$kode);
		$this->db->update('tinovice', $data2);

		$kdJadwal = $this->get_kode_jadwal($kode);

		$this->update_jadwal_fromdb($kdJadwal, 'booking');
	}
	

	public function hapus_keranjang($id)
	{
		$this->db->delete('tboking_temp', array('kdJadwal'=>$id));
		$this->db->delete('tjadwal', array('kdJadwal'=>$id));
	}

	public function batal_booking($kode, $id, $idbook)
	{
		$data = array(); 
		foreach ($kode as $id) {
		   array_push($data, array(
		  'kdJadwal' => $id ,
		  'status' => 'unbook'
		   ));
		} 
		$this->db->delete('tboking', array('kdInovice'=>$id));  
		$this->db->delete('tinovice', array('kdInovice'=>$id));
		$this->db->delete('tboking_info', array('kdBoking'=>$idbook));

		$this->db->update_batch('tjadwal', $data, 'kdJadwal'); 
	}
	
}