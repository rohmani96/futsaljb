<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Schedule extends CI_Model {

	var $table = 'tjadwal'; //nama tabel dari database
    var $column_order = array(null, 'tglBook','kdLap','jamBook','jamSelesai','status'); //field yang ada di table user
    var $column_search = array('tglBook'); //field yang diizin untuk pencarian 
    var $order = array('tglBook' => 'desc', 'jamBook'=>'desc'); // default order 

    private function _get_datatables_query($query=NULL)
    {
         if ($query==NULL) {
            $this->db->from($this->table);
         }else{
            $this->db->from($this->table);
            $this->db->where('status', $query);
         }
        
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        	 {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($query=NULL)
    {
        $this->_get_datatables_query($query);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($query=NULL)
    {
        $this->_get_datatables_query($query);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($query=NULL)
    {
        if ($query==NULL) {
            $this->db->from($this->table);
         }else{
            $this->db->from($this->table);
            $this->db->where('status', $query);
         }
        return $this->db->count_all_results();
    }

	public function get_all()
	{
		
		$this->db->from('tjadwal');
		$this->db->order_by("tglBook desc, jamBook desc");
		$this->db->where('status', 'booked');
		$query = $this->db->get(); 
		return $query->result_array();
	}

	public function get_lapangans()
	{
		$query = $this->db->get('tlapangan');
		return $query->result_array();
	}

	public function get_count_jadwal()
	{
		
		$this->db->from('tjadwal');
		$this->db->where('tglBook', date("Y/m/d"));
		$query = $this->db->get(); 
		return $query->result_array();
	}

	public function save_jadwal()
	{
		$data = array (
			'kdLapangan'=> $this->input->post('lapangan'),
			'hari'  	=> $this->input->post('hari'),
			'harga'     => $this->input->post('harga')
		);

		return $this->db->insert('tjadwal', $data);
	}

	public function update_jadwal($id) {

		$data = array (
			'kdLapangan'=> $this->input->post('lapangan'),
			'hari'  	=> $this->input->post('hari'),
			'harga'  	=> $this->input->post('harga')
		);

		$this->db->where('kdJadwal', $id);
		return $this->db->update('tjadwal', $data);
	}

	public function delete_jadwal($id)
	{
		return $this->db->delete('tjadwal', array('kdJadwal'=>$id));
	}

}